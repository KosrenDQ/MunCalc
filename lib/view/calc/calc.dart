import 'package:flutter/material.dart';

import 'package:mun_calc/style.dart';
import 'package:mun_calc/view/index.dart';

const int PISTOL = 0;
const int RIFLE = 1;
const int BOTH = 2;

class MunCalcPage extends StatefulWidget {
  final String title;
  final int weapon;
  final int index;
  
  MunCalcPage({Key key, this.title, this.weapon, this.index}) : super(key: key);
  
  @override
  _MunCalcPage createState() => new _MunCalcPage();
}
class _MunCalcPage extends State<MunCalcPage> {
  
  Widget _createPistolView() {
    return new Card(
      elevation: 5.0,
      margin: EdgeInsets.only(
        top: Percent.getHeight(context, 0.08),
        bottom: Percent.getHeight(context, 0.02),
        right: Percent.getWidth(context, 0.02),
        left: Percent.getWidth(context, 0.06),
      ),
      child: new Container(
        margin: EdgeInsets.only(
          top: Percent.getHeight(context, 0.02),
          left: Percent.getWidth(context, Percent.per20),
          right: Percent.getWidth(context, Percent.per20)
        ),
        child: new Column(
          children: <Widget>[
            new Text('P8'),
          ],
        )
      )
    );
  }
  
  Widget _createRifleView() {
    return new Card(
      elevation: 5.0,
      margin: EdgeInsets.only(
        top: Percent.getHeight(context, 0.08),
        bottom: Percent.getHeight(context, 0.02),
        left: Percent.getWidth(context, 0.02),
        right: Percent.getWidth(context, 0.06)
      ),
      child: new Container(
        margin: EdgeInsets.only(
          top: Percent.getHeight(context, 0.02),
          left: Percent.getWidth(context, Percent.per20),
          right: Percent.getWidth(context, Percent.per20)
        ),
        child: new Column(
          children: <Widget>[
            new Text('G36'),
          ],
        )
      )
    );
  }
  
  @override
  Widget build(BuildContext context) {
    final List<Widget> children = new List<Widget>();
    switch (this.widget.weapon) {
      case PISTOL:
        children.add(_createPistolView());
        break;
      case RIFLE:
        children.add(_createRifleView());
        break;
      case BOTH:
        children.add(_createPistolView());
        children.add(_createRifleView());
        break;
      default:
        break;
    }
    
    return new Scaffold(
      appBar: new NavBar(title: widget.title),
      body: new Column(
        children: <Widget>[
          new Row(
            children: children
          )
        ]
      )
    );
  }
}