import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:mun_calc/view/index.dart';
import 'package:mun_calc/style.dart';

class StartPage extends StatefulWidget {
  final String title;
  final int index;
  
  StartPage({Key key, this.title, this.index}) : super(key: key);
  
  @override
  _StartPage createState() => new _StartPage();
}

class _StartPage extends State<StartPage> {
  bool _pistol = false;
  bool _rifle = false;
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new NavBar(title: widget.title),
      body: new Card(
        elevation: 5.0,
        margin: EdgeInsets.only(
          top: Percent.getHeight(context, 0.08),
          bottom: Percent.getHeight(context, 0.02),
          left: Percent.getWidth(context, 0.08),
          right: Percent.getWidth(context, 0.08)
        ),
        child: new Container(
          margin: EdgeInsets.only(
            top: Percent.getHeight(context, 0.08),
            left: Percent.getWidth(context, Percent.per20),
            right: Percent.getWidth(context, Percent.per20)
          ),
          child: new Column(
            children: <Widget>[        
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text('Pistole P8'),
                  new Expanded(child: new Container()),
                  new Container(
                    child: new Switch(
                      activeColor: MunStyle.widget_Color,
                      value: _pistol, 
                      onChanged: (bool value) {
                        _pistol = value;
                      }
                    )
                  )
                ]
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text('Gewehr G36'),
                  new Expanded(child: new Container()),
                  new Container(
                    child: new Switch(
                      activeColor: MunStyle.widget_Color,
                      value: _rifle,
                      onChanged: (bool value) {
                        _rifle = value;
                      }
                    )
                  )
                ]
              ),
              new Container(
                width: Percent.getWidth(context, Percent.per60),
                margin: EdgeInsets.only(
                  top: Percent.getHeight(context, Percent.per20)
                ),
                child: new RaisedButton(
                  child: new Text('Start'),
                  textColor: MunStyle.text_Color,
                  elevation: 4.0,
                  onPressed: () {
                    if (_rifle || _pistol) {
                      int weaponKey = _rifle ? (_pistol ? BOTH : RIFLE) : PISTOL;
                      Navigator.of(context).push(
                        new CupertinoPageRoute(
                            builder: (_) => new MunCalcPage(title: 'Munitionswächter', weapon: weaponKey, index: 2)
                        )
                      );
                    } else {
                      Fluttertoast.showToast(msg: 'Mindestens eine Waffe auswählen');
                    }
                  }
                )
              ),
              new Container(
                width: Percent.getWidth(context, Percent.per60),
                child: new RaisedButton(
                  child: new Text('Einstellungen'),
                  textColor: MunStyle.text_Color,
                  elevation: 4.0,
                  onPressed: () {
                    Navigator.of(context).push(
                      new MaterialPageRoute(
                        builder: (_) => new SettingsPage(title: 'Einstellungen', index: 2)
                      )
                    );
                  },
                )
              )
            ]
          )
        )
      )
    );
  }
}