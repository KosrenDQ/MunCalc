import 'package:flutter/material.dart';

import 'package:mun_calc/style.dart';

class NavBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  final String title;
  
  NavBar({Key key, this.title}) : preferredSize = new Size.fromHeight(kToolbarHeight), super(key: key);
  
  @override
  _NavBar createState() => new _NavBar();
}

class _NavBar extends State<NavBar> {
  
  @override
  Widget build(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: new Text(
        this.widget.title,
        style: new TextStyle(
          color: MunStyle.text_Color
        ),
      ),
    );
  }
}