import 'package:flutter/material.dart';

import 'package:mun_calc/model/index.dart';
import 'package:mun_calc/view/index.dart';
import 'package:mun_calc/style.dart';

class SettingsPage extends StatefulWidget {
  final String title;
  final int index;
  
  SettingsPage({Key key, this.title, this.index}) : super(key: key);
  
  @override
  _SettingsPage createState() => new _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {
  String _munPistol;
  String _munRifle;
  
  @override
  void initState() {
    super.initState();

    this._munPistol = MunCalcConfig.munPistol.toString();
    this._munRifle = MunCalcConfig.munRifle.toString();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: NavBar(title: this.widget.title),
      body: new Card(
        elevation: 5.0,
        margin: EdgeInsets.only(
          top: Percent.getHeight(context, 0.08),
          bottom: Percent.getHeight(context, 0.02),
          left: Percent.getWidth(context, 0.08),
          right: Percent.getWidth(context, 0.08)
        ),
        child: new Container(
          margin: EdgeInsets.only(
            top: Percent.getHeight(context, 0.08),
            left: Percent.getWidth(context, Percent.per20),
            right: Percent.getWidth(context, Percent.per20)
          ),
          child: new Column(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text('Schuss pro Paket / P8'),
                  new Expanded(child: new Container()),
                  new Container(
                    width: Percent.getWidth(context, 0.05),
                    child: new TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      controller: new TextEditingController(text: this._munPistol),
                      onChanged: (value) => this._munPistol = value,
                    )
                  )
                ]
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text('Schuss pro Gewehr / G36'),
                  new Expanded(child: new Container()),
                  new Container(
                    width: Percent.getWidth(context, 0.05),
                    child: new TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      controller: new TextEditingController(text: this._munRifle),
                      onChanged: (value) => this._munRifle = value,
                    )
                  )
                ]
              ),
              new Container(
                width: Percent.getWidth(context, Percent.per60),
                margin: EdgeInsets.only(
                  top: Percent.getHeight(context, Percent.per20)
                ),
                child: new RaisedButton(
                  child: new Text('Speichern'),
                  textColor: MunStyle.text_Color,
                  elevation: 4.0,
                  onPressed: () {
                    MunCalcConfig.munPistol = int.parse(this._munPistol);
                    MunCalcConfig.munRifle = int.parse(this._munRifle);
                  }
                )
              )
            ]
          )
        )
      )
    );
  }
}