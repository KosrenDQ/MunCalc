import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:mun_calc/style.dart';
import 'package:mun_calc/view/index.dart';

void main() {
  SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Mun Rechner',
      theme: munStyle,
      home: new StartPage(title: 'Neues Schießen erstellen', index: 1),
    );
  }
}
