import 'package:flutter/material.dart';

ThemeData munStyle = new ThemeData(
  primaryColor: Color(0xFF424632),
  primaryIconTheme: IconThemeData(color: Color(0xFFDDDDD9)),
  buttonColor: Color(0xFF5E6250),
  cardColor: Color(0xFFDDDDD9),
  scaffoldBackgroundColor: Color(0xFF47402E)
);

class MunStyle {
  static const Color widget_Color = Color(0xFF5E6250);
  static const Color text_Color = Color(0xFFDDDDD9);
} 

class Percent {
  static final double per100 = 1.0;
  static final double per90 = 0.9;
  static final double per80 = 0.8;
  static final double per75 = 0.75;
  static final double per70 = 0.7;
  static final double per60 = 0.6;
  static final double per50 = 0.5;
  static final double per40 = 0.4;
  static final double per30 = 0.3;
  static final double per25 = 0.25;
  static final double per20 = 0.2;
  static final double per15 = 0.15;
  static final double per10 = 0.1;
  static final double per00 = 0.0;

  static double getWidth(BuildContext context, double value){
    return MediaQuery.of(context).size.width * value;
  }

  static double getHeight(BuildContext context, double value){
    return MediaQuery.of(context).size.height * value;
  }
}